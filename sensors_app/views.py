from models import GetData, InsertDataIntoDb, GetActualCityWeather, WEATHER_API_BASE_URL, GetSensorsAPIJsonData,  SENSORS_API_BASE_URL
from json import JSONEncoder
from tinydb import TinyDB, Query
from flask import Flask, render_template, url_for, jsonify


app = Flask(__name__)

app.config.from_object('config')

description_projet = """
    Ce challenge Battle Dev Ril avait pour objectif principal de réaliser une station météo connectée basique à partir d'un Web Service fournissant des données de températures et humidités et à base du langage Python, UNIQUEMENT Python. Le Web Service délivre des données brutes au format HEXA. Nous avions une documentation, RD06 WIFI data protocol-v1.1.pdf, afin d'utiliser les données fournies. L'url du Web Service est : http://app.objco.com:8099/?account=[CODE ACCOUNT]&limit=[LIMIT]. Le code account est fourni pour chaque groupe par l'intervenant. Le paramètre limit permet de définir le nombre des derniers relevés. LeFormat de la réponse est en JSON.
    """

actual_api_weather_data = GetActualCityWeather(
    WEATHER_API_BASE_URL, app.config['WEATHER_API_KEY'])


@app.route('/')
@app.route('/index/')
def index():
    page_title = "CHALLENGE BATTLE DEV RIL PYTHON ! - Accueil"
    og_url = url_for('index', img="", _external=True)
    og_image = url_for('static', filename='')
    og_description = "Challenge battle dev réalisé avec les RILA"

    return render_template('index.html',
                           authors_name="Imade Benbadda & Thomas Maulon",
                           authors_picture=url_for(
                               'static', filename='img/profile.png'),
                           description=description_projet,
                           blur=True,
                           page_title=page_title,
                           og_url=og_url,
                           og_image=og_image,
                           og_description=og_description,
                           weather_data=actual_api_weather_data
                           )


@app.route('/results/')
def results():
    SENSORS_API_KEY = app.config['SENSORS_API_KEY']
    API_RESULTS_LIMIT = "4"
    API_URL = 'http://app.objco.com:8099/?account=' + SENSORS_API_KEY + \
        '&limit=' + API_RESULTS_LIMIT
    sensors_api_json_data = GetSensorsAPIJsonData(API_URL)
    InsertDataIntoDb(sensors_api_json_data)
    page_title = "CHALLENGE BATTLE DEV RIL PYTHON ! - Résultats"
    og_url = url_for('index', img="", _external=True)
    og_image = url_for('static', filename='')
    og_description = "Challenge battle dev réalisé avec les RILA"

    stored_data = GetData(4)

    def getDates(datas):
        dates = []
        for data in datas:
            date: str = data.get('date_time')
            dates.append(date)
        return dates
    dates = getDates(GetData(10))

    def getHumidityValues(datas):
        humidities_values = []
        for data in datas:
            sensors = data.get('sensors_informations')
            for sensor in sensors:
                humidities_values.append(
                    sensor.get('sensor_humidity_decrypt'))
                humidities_values_filtered = list(
                    filter(lambda x: type(x) is int, humidities_values))
        return humidities_values_filtered
    humidities_values = getHumidityValues(GetData(10))

    def getTemperatureValues(datas):
        temperature_values = [[], []]
        for data in datas:
            sensors = data.get('sensors_informations')
            for x in range(len(sensors)):
                temperature_values[x-1].append(
                    round(sensors[x].get('sensor_temperature_decrypt'), 2))
        return temperature_values
    temperature_values = getTemperatureValues(GetData(10))

    # last 2 hours values : 12*5*2
    historical_data = GetData(120)

    return render_template('results.html',
                           authors_name="Imade Benbadda & Thomas Maulon",
                           authors_picture=url_for(
                               'static', filename='img/profile.png'),
                           description=description_projet,
                           page_title=page_title,
                           og_url=og_url,
                           og_image=og_image,
                           og_description=og_description,
                           data_raws=stored_data,
                           chart_date_labels=dates,
                           chart_humidities_values=humidities_values,
                           chart_temperature_values=temperature_values,
                           table_historical_values=historical_data,
                           weather_data=actual_api_weather_data
                           )


if __name__ == "__main__":
    app.run()
