import smtplib  # Importation du module
from tinydb import TinyDB, Query
import requests
import json

from config import WITH_ACCOUNT, WITH_ACCOUNT_PASSWORD

SENSORS_API_BASE_URL = 'http://app.objco.com:8099/?account='


def GetSensorsAPIJsonData(url):
    response = requests.get(url)
    response_text = response.text
    print(response_text)
    json_data = json.loads(response_text)
    return json_data


def createMetricsWithResponses(raws):
    metrics = {}
    for x in range(len(raws)):
        response_id: int = x
        metric_id: int = raws[x][0]
        wifi_protocol_data_code: str = raws[x][1]
        metric_date_time: str = raws[x][2]
        metrics.update({
            response_id: {
                0: metric_id,
                1: wifi_protocol_data_code,
                2: metric_date_time,
            }
        })
    return metrics


def find_sensor_informations(wifi_protocol_data_code: str, sensor_ids: list):
    sensor_informations: list = []
    for sensor_id in sensor_ids:
        if wifi_protocol_data_code.find(sensor_id) != None:
            # id
            sensor_id_position = wifi_protocol_data_code.find(sensor_id)
            sensor_id_length: str = len(sensor_id_1)

            # status
            sensor_status_length = 2
            sensor_status_start_index = sensor_id_position + sensor_id_length
            sensor_status_end_index = sensor_status_start_index + sensor_status_length
            sensor_status = wifi_protocol_data_code[sensor_status_start_index:sensor_status_end_index]

            # voltage
            sensor_battery_voltage_length = 4
            sensor_battery_voltage_start_index = sensor_status_end_index
            sensor_battery_voltage_end_index = sensor_battery_voltage_start_index + \
                sensor_battery_voltage_length
            sensor_battery_voltage = wifi_protocol_data_code[
                sensor_battery_voltage_start_index:sensor_battery_voltage_end_index]
            sensor_battery_voltage_decrypt = (int(
                sensor_battery_voltage, base=16) * 10) / 1000

            # temperature
            sensor_temperature_length = 4
            sensor_temperature_start_index = sensor_battery_voltage_end_index
            sensor_temperature_end_index = sensor_temperature_start_index + \
                sensor_temperature_length
            sensor_temperature = wifi_protocol_data_code[
                sensor_temperature_start_index: sensor_temperature_end_index]

            def set_temperature_sign(temperature_hexa_code):
                if(int(temperature_hexa_code[0], base=16) == 0):
                    return 1
                elif(int(temperature_hexa_code[0], base=16) == 4):
                    return -1
                else:
                    return 0
            sensor_temperature_decrypt = round(
                int(sensor_temperature[2:4], base=16)*set_temperature_sign(sensor_temperature)*0.1, 2)

            # humidity
            sensor_humidity_length = 2
            sensor_humidity_start_index = sensor_temperature_end_index
            sensor_humidity_end_index = sensor_humidity_start_index + \
                sensor_humidity_length
            sensor_humidity = wifi_protocol_data_code[
                sensor_humidity_start_index: sensor_humidity_end_index]

            def set_humidity_value(humidity_hexa_code):
                if(humidity_hexa_code == 'FF'):
                    return "n.a"
                else:
                    return int(hex(
                        int(sensor_humidity, base=16)), base=16)
            sensor_humidity_decrypt = set_humidity_value(sensor_humidity)

            # rssi
            sensor_rssi_length = 2
            sensor_rssi_start_index = sensor_humidity_end_index
            sensor_rssi_end_index = sensor_rssi_start_index + \
                sensor_rssi_length
            sensor_rssi = wifi_protocol_data_code[
                sensor_rssi_start_index: sensor_rssi_end_index]
            sensor_rssi_decrypt = int(hex(
                int(sensor_rssi, base=16)), base=16)*-1

            sensor_informations.append(
                {
                    'sensor_id': sensor_id,
                    'sensor_battery_voltage_decrypt': sensor_battery_voltage_decrypt,
                    'sensor_temperature_decrypt': sensor_temperature_decrypt,
                    'sensor_humidity_decrypt': sensor_humidity_decrypt,
                    'sensor_rssi_decrypt': sensor_rssi_decrypt,
                })
    return sensor_informations


sensor_id_1: str = '62182233'
sensor_id_2: str = '06182660'
sensor_ids = []
sensor_ids.append(sensor_id_1)
sensor_ids.append(sensor_id_2)


def createResponsesDataArrayWithSensorsInformations(raws):
    raws_to_insert = []
    for x in range(len(raws)):
        metric_id: int = raws[x][0]
        wifi_protocol_data_code: str = raws[x][1]
        metric_date_time: str = raws[x][2]
        sensors_informations = find_sensor_informations(
            wifi_protocol_data_code, sensor_ids)
        raws_to_insert.append({
            'id': metric_id,
            'wifi_protocol_data_code': wifi_protocol_data_code,
            'date_time': metric_date_time,
            'sensors_informations': sensors_informations
        })
    return raws_to_insert


def FetchSensorsData(json_data):
    responses_to_insertWithSensorsInformations = createResponsesDataArrayWithSensorsInformations(
        json_data)
    return responses_to_insertWithSensorsInformations


db = TinyDB('db.json')


def InsertDataIntoDb(json_data):
    responses_to_insertWithSensorsInformations = FetchSensorsData(json_data)
    for response in responses_to_insertWithSensorsInformations:
        db.insert(response)


def GetData(limit: int):
    stored_data = db.all()
    return stored_data[-limit:]


WEATHER_API_BASE_URL = 'http://api.openweathermap.org/data/2.5/weather?q='


def GetActualCityWeather(API_BASE_URL, API_KEY):
    url = API_BASE_URL + "bordeaux" + "&appid=" + API_KEY
    response_weather = requests.get(url)
    data = response_weather.json()

    temperature_city = data['name']
    temperature_main = data['weather'][0]['main']
    temperature_description = data['weather'][0]['description']
    temperature_icon = data['weather'][0]['icon']

    temperature_kelvin = data['main']['temp']
    temperature_celcius = round(temperature_kelvin - 273.15, 2)
    temperature_humidity = data['main']['humidity']

    actual_city_weather_data = {
        "temperature_city": temperature_city,
        "temperature_main": temperature_main,
        "temperature_description": temperature_description,
        "temperature_icon": temperature_icon,
        "temperature_celcius": temperature_celcius,
        "temperature_humidity": temperature_humidity,
    }
    print(actual_city_weather_data)
    return actual_city_weather_data


TO_PROD_LIST = ["thomas.maulon@viacesi.fr",
                "imade.benbadda@viacesi.fr", "eric.artigala@gmail.com"]

TO_TEST_LIST = ["thomas.maulon@viacesi.fr", "imade.benbadda@viacesi.fr"]

MESSAGE = """\
    Subject: Sensors alert

    This message is sent because of unusual values concerning your sensors have been detected."""


def SendMailAlert(WITH_ACCOUNT, WITH_ACCOUNT_PASSWORD, TO_LIST, MESSAGE, TEMPERATURE_TRESHOLD):
    stored_data = GetData(1)
    current_temperatures = []
    for data in stored_data:
        for sensor in data['sensors_informations']:
            current_temperatures.append(sensor['sensor_temperature_decrypt'])
    print(current_temperatures)

    alert_temperatures = []
    for current_temperature in current_temperatures:
        if(current_temperature < TEMPERATURE_TRESHOLD):
            alert_temperatures.append(current_temperature)

    if(len(alert_temperatures) > 0):
        alert_message = MESSAGE + ' Temperature(s) : ' + ' '.join(
            map(lambda x: str(x) + '°C', alert_temperatures))
        print(MESSAGE + alert_message)
        # Connexion au serveur sortant (en précisant son nom et son port)
        serveur = smtplib.SMTP('smtp.gmail.com', 587)
        serveur.starttls()  # Spécification de la sécurisation
        serveur.login(WITH_ACCOUNT,
                      WITH_ACCOUNT_PASSWORD)  # Authentification
        serveur.sendmail(WITH_ACCOUNT,
                         TO_LIST, MESSAGE)
        serveur.quit()  # Déconnexion du serveur


SendMailAlert(WITH_ACCOUNT, WITH_ACCOUNT_PASSWORD,
              TO_TEST_LIST, MESSAGE, 10)
